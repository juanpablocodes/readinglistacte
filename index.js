/* 1. Create a readingListActE folder. Inside create an index.js file.
2. Install nodemon and perform npm init command in gitbash to create a package.json file.
3. Install express and mongoose. Make sure to check these dependencies insde the package.json file.
4. Create a .gitignore file and store the node_modules in it.
5. Once done with your solution, create a repo named "readingListActE" and push your solution.
6. Save the repo link on S35-C1: Express.js: Data Persistende via Mongoos ODM */

/*

Activity Instructions:
1. Create a User schema with the following fields: firstName, lastName, username, password, email.
2. Create a Product schema with the following fields: name, description, price.
3. Create a User Model and  a Product Model.
4. Create a POST request that will access the /register route which will create a user. Test this in Postman app.
5. Create a POST request that will access the /createProduct route which will create a product. Test this in Postman app.
6. Create a GET request that will access the /users route to retrieve all users from your DB. Test this in Postman.
7. Create a GET request that will access the /products route to retrieve all products from your DB. Test this in Postman.

Note: create a separate Database collection named S35-C1 in your MongoDb account.
*/


const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 2000;

mongoose.connect(`mongodb+srv://juanpablocodes:<password>@clusterbatch-197.xubg08v.mongodb.net/readingListActE?retryWrites=true&w=majority`, {
	
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connection to MongoDB!'))
